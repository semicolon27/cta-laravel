<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ URL::asset('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/assets/css/login.css') }}">
    <link rel="stylesheet" href="">
    <link rel="stylesheet" href="">
    <title>Login</title>
    <link href="" rel="stylesheet" type="text/css"/>
    <link href="" rel="stylesheet" type="text/css"/>
    <script src="{{ URL::asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/jquery-3.3.1.js') }}"></script>
</head>

<body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first">
                    <h5 style="color:gray">Login</h5>
                </div>
                @if(\Session::get('error'))
                <div class="error">{{ \Session::get('error') }}</div>
                @endif
                <form action="{{ URL::action('LoginController@login') }}" method="POST">
                    @csrf
                    <input type="text" id="login" class="fadeIn second" name="username"
                        placeholder="Type your username">
                    <input type="text" id="password" class="fadeIn third" name="password"
                        placeholder="Type your password">
                    <input type="submit" class="fadeIn fourth" value="Log In">
                </form>
            </div>
        </div>

</body>

</html>
