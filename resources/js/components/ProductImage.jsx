import React, { Component } from 'react';
import AddImage from './Modals/AddImage';
import ImageModal from './Modals/ImageModal';

class ProductImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            img: []
         };

    }

    componentDidMount(){
        fetch('/api/gallery/'+this.props.id)
        .then(res => res.json())
        .then(data => this.setState({ img : data.image }))
      }

    render() {
        const { img } = this.state
        console.log(img)
        return (
            <React.Fragment>
                <div className="jumbotron">
                    <h1 align="center">Gambar Produk</h1>
                </div>
                <div className="container">
                    <div className="row">
                        {img.map((item, key) => {
                            return (
                                <div className="col-md-4">
                                    <div className="thumbnail">
                                        <a href={`/assets/img/product/${item.img}`} target="_blank">
                                            <img src={`/assets/img/product/${item.img}`} alt="Lights"
                                                style={{ width: "100%" }} />
                                            <div className="middle" id="option">
                                                <form action="/admin/product/delete_img" method="post">
                                                    {/* <input type="hidden" name="p_id" value="{{product_id}}" /> */}
                                                    {/* <input type="hidden" name="id" value="{{img_id}}" /> */}
                                                    {/* <input type="submit" value="delete" className="btn btn-danger" onClick="return confirm('Are You Sure Want To Delete This Image ?')" /> */}
                                                </form>
                                            </div>
                                        </a>
                                        <ImageModal name={ item.img } dir="/assets/img/product/" id={item.img_id} />
                                        <div className="caption">
                                            <p>{item.note}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                        { (img.length < 6) ? (
                                <div class="col-md-4">
                                    <div class="thumbnail" id="myimage">
                                        <AddImage id={this.props.id} />
                                    </div>
                                </div>
                            ) : ""}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default ProductImage;
