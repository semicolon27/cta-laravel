// resources/assets/js/components/ProjectsList.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AddProductModal from './Modals/AddProductModal';
import EditProductModal from './Modals/EditProductModal';
import ImageModal from './Modals/ImageModal';
import ConfirmDelete from './Modals/ConfirmDelete';

class ProductList extends Component {

  constructor () {
    super()
    this.state = {
      products: []
    }
  }


//   componentDidMount () {
//     axios.request({
//         method: 'GET',
//         url: `http://localhost:8000/api/product`,
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json',
//         }
//     }).then(response => {
//       this.setState({
//         products: response.results
//       })
//     })
//   }
  componentDidMount(){
    fetch('http://localhost:8000/api/product')
    .then(res => res.json())
    .then(data => this.setState({ products : data.results }))
  }
  render () {
    const { products } = this.state
    let no = 0;

    console.log(products)
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <a className="navbar-brand" href="#">&nbsp;&nbsp;Product List</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/admin/product"><span className="nav-link active">Product<span className="sr-only">(current)</span></span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="/admin/service"><span className="nav-link">Service</span></Link>
                        </li>
                        <li className="nav-item">
                            <a href="/logout"><span className="nav-link">Logout</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <br />
            <div className="container">
            <AddProductModal />
                <table className="table table-striped" id="mytable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Image</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map((item, key) => {
                        return (
                        <tr key="{key}">
                            <td>{ no++ }</td>
                            <td>{ item.product_name }</td>
                            <td className="descField">{ item.product_description }</td>
                            <td>Rp.{ item.product_price }</td>
                            <td><ImageModal name={ item.image } dir="/assets/img/product/" /></td>
                            <td>
                                <Link to={`/admin/product/${item.product_id}`}>Detail
                                </Link>
                            </td>
                            <td>
                            <EditProductModal product_id={ item.product_id } product_name={ item.product_name } product_description={ item.product_description } product_price={ item.product_price } image={item.image}/>
                            <ConfirmDelete id={item.product_id} type="product" title="Product" />
                            </td>
                        </tr>
                        )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
  }
}

export default ProductList
