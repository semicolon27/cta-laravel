import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';

export default class AddProductModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        product_name: "",
        product_description : "",
        product_price: 0,
        image: ""
    }
    this.toggle = this.toggle.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDesc = this.handleChangeDesc.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleChangeImage = this.handleChangeImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  handleChangeName(event) {
    this.setState({product_name: event.target.value});
  }
  handleChangeDesc(event) {
    this.setState({product_description: event.target.value});
  }
  handleChangePrice(event) {
    this.setState({product_price: event.target.value});
  }
  handleChangeImage(e) {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length)
          return;
    this.createImage(files[0]);
  }
  createImage(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        image: e.target.result
      })
    };
    reader.readAsDataURL(file);
  }
  handleSubmit(event) {
    event.preventDefault();
    const form = {
        product_id: parseInt(Math.random() * 9999 /3 *4),
        product_name: this.state.product_name,
        product_description: this.state.product_description,
        product_price: this.state.product_price,
        file: this.state.image
    }
    console.log(form);
    let uri = 'http://localhost:8000/api/product';
    axios.post(uri, form).then((response) => {
     this.setState({
        modal: !this.state.modal
      });
    });
  }


  render() {
    return (

        <React.Fragment>
        <Button color="success" className="btn btn-block" onClick={this.toggle}>Add Product</Button>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit} enctype="multipart/form-data">
          <ModalHeader>Tambah Produk</ModalHeader>
          <ModalBody>
            <div className="form-group">
            <label>Product Name:</label>
            <input type="text" value={this.state.product_name} onChange={this.handleChangeName} className="form-control" />
              </div>
             <div className="form-group">
            <label>Product Description:</label>
                <input type="text" value={this.state.product_description} onChange={this.handleChangeDesc} className="form-control" />
              </div>
             <div className="form-group">
            <label>Product Price:</label>
                <input type="text" value={this.state.product_price} onChange={this.handleChangePrice} className="form-control" />
              </div>
             <div className="form-group">
              <label>Image:</label>
                <input type="file" onChange={this.handleChangeImage} className="form-control" />
               </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
          </form>
        </Modal>
        </React.Fragment>

    );
  }
}
