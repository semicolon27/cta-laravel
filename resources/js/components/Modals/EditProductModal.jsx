import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';

export default class EditProductModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        product_id: props.product_id,
        product_name: props.product_name,
        product_description : props.product_description,
        product_price: props.product_price,
        image: props.image
    }
    this.toggle = this.toggle.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDesc = this.handleChangeDesc.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
    this.handleChangeImage = this.handleChangeImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  handleChangeName(event) {
    this.setState({product_name: event.target.value});
  }
  handleChangeDesc(event) {
    this.setState({product_description: event.target.value});
  }
  handleChangePrice(event) {
    this.setState({product_price: event.target.value});
  }
  handleChangeImage(event) {
    this.setState({image: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();
    const form = {
        product_name: this.state.product_name,
        product_description: this.state.product_description,
        product_price: this.state.product_price,
        image: this.state.image
    }
    console.log(form);
    let id = this.state.product_id;
    console.log(id);
    let uri = '/api/product/'+id+'/edit';
    axios.put(uri, form).then((response) => {
    alert("Data Berhasil Diinput !!");
     this.setState({
        modal: !this.state.modal
      });
    });
  }

  render() {
    return (

        <div>
        <Button color="primary" onClick={this.toggle}>Edit</Button>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit}>
          <ModalHeader>Product Edit</ModalHeader>
          <ModalBody>
            <div className="form-group ">
                <label>Product Name:</label>
                <input type="text" value={this.state.product_name} onChange={this.handleChangeName}
                    className="form-control" />
            </div>
            <div className="form-group ">
                <label>Product Description:</label>
                <textarea onChange={this.handleChangeDesc} className="form-control">{this.state.product_description}</textarea>
            </div>
            <div className="form-group ">
                <label>Product Price:</label>
                <input type="text" value={this.state.product_price} onChange={this.handleChangePrice}
                    className="form-control" />
            </div>
            <div className="form-group ">
                <label>Image:</label>
                <input type="text" value={this.state.image} onChange={this.handleChangeImage} className="form-control" />
            </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
          </form>
        </Modal>
        </div>

    );
  }
}
