import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ConfirmDelete from './ConfirmDelete';

export default class ImageModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        img_name: props.name,
        img_dir: props.dir
    }
    this.toggle = this.toggle.bind(this);
    }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
      let img_area = {
          size: {
            height: '50vh !important',
            width: '50vh !important'
          }
      }
    return (

        <div>
        <Button color="primary" onClick={this.toggle}>{ this.state.img_name }</Button>
        <Modal isOpen={this.state.modal} style={img_area.size}>
          <ModalHeader>{this.state.img_name}</ModalHeader>
          <ModalBody style={img_area.size}>
            <div style={img_area.size}>
                <img src={`${this.state.img_dir}/${this.state.img_name}`} alt="gambar" />
            </div>
          </ModalBody>
          <ModalFooter>
            {/* {(this.props.match) ? ( <ConfirmDelete id={this.props.id} type="image" title="Image" /> ): ""} */}
            {/* {console.log} */}
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
        </div>

    );
  }
}
