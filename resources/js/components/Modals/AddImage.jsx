import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';
import uniqid from 'uniqid';

class AddImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            img: "",
            note: "",
         };
        this.toggle = this.toggle.bind(this);
        this.handleChangeImg = this.handleChangeImg.bind(this);
        this.handleChangeNote = this.handleChangeNote.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

      handleChangeNote(event) {
        this.setState({note: event.target.value});
      }
      handleChangeImg(e) {
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length)
            return;
        this.createImage(files[0]);
      }

      createImage(file) {
        let reader = new FileReader();
        reader.onload = (e) => {
          this.setState({
            img: e.target.result
          })
        };
        reader.readAsDataURL(file);
      }

      handleSubmit(event) {
        event.preventDefault();
        const form = {
            img_id: uniqid.time(),
            product_id: this.props.id,
            file: this.state.img,
            note: this.state.note,
        }
        console.log(form);
        let uri = '/api/product/gallery/'+this.props.id;
        axios.post(uri, form).then((response) => {
         this.setState({
            modal: !this.state.modal
          });
        })
        .catch((error) => {
            console.log(error);
        });
      }

    render() {
        return (
            <React.Fragment>
                <Button color="success" className="btn btn-block" onClick={this.toggle}>Add Image</Button>
                <Modal isOpen={this.state.modal}>
                <form onSubmit={this.handleSubmit} encType="multipart/form-data">
                <ModalHeader>Tambah Gambar</ModalHeader>
                <ModalBody>
                    <div className="form-group">
                    <label>Image:</label>
                        <input type="file" onChange={this.handleChangeImg} className="form-control" />
                    </div>
                    <div className="form-group">
                    <label>Note:</label>
                        <input type="text" value={this.state.note} onChange={this.handleChangeNote} className="form-control" />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
                    <Button color="danger" onClick={this.toggle}>Cancel</Button>
                </ModalFooter>
                </form>
                </Modal>
            </React.Fragment>
        );
    }
}

export default AddImage;
