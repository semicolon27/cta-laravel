import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';

export default class ConfirmDelete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
    }
    this.toggle = this.toggle.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const form = {
        id: this.props.id
    }
    let uri = 'http://localhost:8000/api/'+this.props.type+'/delete';
    axios.post(uri, form).then((response) => {
     this.setState({
        modal: !this.state.modal
      });
      alert("Data Berhasil Dihapus")
    });
  }


  render() {
    return (

        <React.Fragment>
            <Button color="danger" className="btn btn-sm btn-danger" onClick={this.toggle}>Delete</Button>
            <Modal isOpen={this.state.modal}>
                <form onSubmit={this.handleSubmit} enctype="multipart/form-data">
                    <ModalHeader>Delete {this.props.title}</ModalHeader>
                    <ModalBody>
                        <div className="modal-body">
                            <strong>Are You Sure To Delete This Data?</strong>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <input type="submit" value="Submit" color="danger" className="btn btn-danger" />
                        <Button onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </form>
            </Modal>
        </React.Fragment>

    );
  }
}
