import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';

export default class EditServiceModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        id_service: props.id_service,
        svc_name: props.svc_name,
        svc_desc : props.svc_desc,
        image: props.image
    }
    this.toggle = this.toggle.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDesc = this.handleChangeDesc.bind(this);
    this.handleChangeImage = this.handleChangeImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  handleChangeName(event) {
    this.setState({svc_name: event.target.value});
  }
  handleChangeDesc(event) {
    this.setState({svc_desc: event.target.value});
  }
   handleChangeImage(event) {
    this.setState({image: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();
    const form = {
        svc_name: this.state.svc_name,
        svc_desc: this.state.svc_desc,
        image: this.state.image
    }
    console.log(form);
    let id = this.state.id_service;
    console.log(id);
    let uri = '/api/service/'+id+'/edit';
    axios.put(uri, form).then((response) => {
    alert("Data Berhasil Diinput !!");
     this.setState({
        modal: !this.state.modal
      });
    });
  }

  render() {
    return (

        <div>
        <Button color="primary" onClick={this.toggle}>Edit</Button>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit}>
          <ModalHeader>Product Edit</ModalHeader>
          <ModalBody>
            <div className="form-group ">
                <label>Product Name:</label>
                <input type="text" value={this.state.svc_name} onChange={this.handleChangeName}
                    className="form-control" />
            </div>
            <div className="form-group ">
                <label>Product Description:</label>
                <textarea onChange={this.handleChangeDesc} className="form-control">{this.state.svc_desc}</textarea>
            </div>
            <div className="form-group ">
                <label>Image:</label>
                <input type="text" value={this.state.image} onChange={this.handleChangeImage} className="form-control" />
            </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
          </form>
        </Modal>
        </div>

    );
  }
}
