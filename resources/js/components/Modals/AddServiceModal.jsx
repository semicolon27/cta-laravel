import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';

export default class AddServiceModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        svc_name: "",
        svc_desc : "",
        image: ""
    }
    this.toggle = this.toggle.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDesc = this.handleChangeDesc.bind(this);
    this.handleChangeImage = this.handleChangeImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  handleChangeName(event) {
    this.setState({svc_name: event.target.value});
  }
  handleChangeDesc(event) {
    this.setState({svc_desc: event.target.value});
  }
  handleChangePrice(event) {
    this.setState({svc_price: event.target.value});
  }
  handleChangeImage(event) {
    this.setState({image: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();
    const form = {
        svc_id: parseInt(Math.random() * 9999 /3 *4),
        svc_name: this.state.svc_name,
        svc_description: this.state.svc_desc,
        svc_price: this.state.svc_price,
        image: this.state.image
    }
    console.log(form);
    let uri = 'http://localhost:8000/api/service';
    axios.post(uri, form).then((response) => {
     this.setState({
        modal: !this.state.modal
      });
    });
  }


  render() {
    return (

        <div>
        <Button color="success" className="btn btn-block" onClick={this.toggle}>Add Service</Button>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit}>
          <ModalHeader>Add Service</ModalHeader>
          <ModalBody>
            <div className="form-group ">
            <label>Service Name:</label>
            <input type="text" value={this.state.svc_name} onChange={this.handleChangeName} className="form-control" />
              </div>
             <div className="form-group ">
            <label>Service Description:</label>
                <input type="text" value={this.state.svc_description} onChange={this.handleChangeDesc} className="form-control" />
               </div>
             <div className="form-group ">
              <label>Image:</label>
                <input type="text" value={this.state.image} onChange={this.handleChangeImage} className="form-control" />
               </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
          </form>
        </Modal>
        </div>

    );
  }
}
