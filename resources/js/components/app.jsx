import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ProductList from './ProductList';
import ServiceList from './ServiceList';
import ProductDetail from './ProductDetail';
import Login from './Login';

    function NoMatch(){
        return <h1>404</h1>
    }
    class App extends Component {
      render () {
        return (
          <BrowserRouter>
            <div>
                <Switch>
                    <Route exact path='/admin/product' component={ProductList} />
                    <Route exact path='/admin/product/:id' component={ProductDetail} />
                    <Route exact path='/admin/service' component={ServiceList} />
                    <Route component={NoMatch} />

                </Switch>

            </div>
          </BrowserRouter>
        )
      }
    }

    ReactDOM.render(<App />, document.getElementById('app'))
