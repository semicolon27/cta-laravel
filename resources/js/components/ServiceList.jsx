// resources/assets/js/components/ProjectsList.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AddServiceModal from './Modals/AddProductModal';
import EditServiceModal from './Modals/EditProductModal';
import ImageModal from './Modals/ImageModal';
import ConfirmDelete from './Modals/ConfirmDelete';

class ProductList extends Component {

  constructor () {
    super()
    this.state = {
      services: []
    }
  }


//   componentDidMount () {
//     axios.request({
//         method: 'GET',
//         url: `http://localhost:8000/api/product`,
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json',
//         }
//     }).then(response => {
//       this.setState({
//         products: response.results
//       })
//     })
//   }
  componentDidMount(){
    fetch('/api/service')
    .then(res => res.json())
    .then(data => this.setState({ services : data.results }))
  }
  render () {
    const { services } = this.state
    let no = 0;

    console.log(services)
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <a className="navbar-brand" href="#">&nbsp;&nbsp;Services List</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/admin/product"><span className="nav-link">Product</span></Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="/admin/service"><span className="nav-link active">Service<span
                                    className="sr-only">(current)</span></span></Link>
                        </li>
                    </ul>
                </div>
            </nav>
            <br />
            <div className="container">
                <AddServiceModal />
                <table className="table table-striped" id="mytable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Service Name</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {services.map((item, key) => {
                        return (
                        <tr key="{key}">
                            <td>{ no++ }</td>
                            <td>{ item.svc_name }</td>
                            <td className="descField">{ item.svc_desc }</td>
                            <td><ImageModal name={ item.image } dir="/assets/img/service/" /></td>
                            <td>
                                <Link to="/product/detail/{item.product_id}">
                                </Link>
                            </td>
                            <td>
                                <EditServiceModal id_service={ item.id_service } svc_name={ item.svc_name }
                                    svc_description={ item.svc_desc } svc_price={ item.svc_price } image={item.image} />
                                <ConfirmDelete id={item.id_service} type="service" title="Service" />
                            </td>
                        </tr>
                        )
                        })}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    )
  }
}

export default ProductList
