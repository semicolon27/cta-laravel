import React, { Component } from 'react'
import ProductImage from './ProductImage';
import { Link } from 'react-router-dom';

class ProductDetail extends Component {

  constructor (props) {
    super(props)
    this.state = {
      product: [],
      image: []
    }
  }

//   componentDidMount () {
//     axios.request({
//         method: 'GET',
//         url: `http://localhost:8000/api/product`,
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json',
//         }
//     }).then(response => {
//       this.setState({
//         products: response.results
//       })
//     })
//   }
componentDidMount(){
    fetch('/api/product/'+this.props.match.params.id)
    .then(res => res.json())
    .then(data => this.setState({ product : data.product }))
  }
  render () {
    const { product } = this.state
    let no = 0;

    console.log(product)
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <Link to="/admin/product"><img src="/assets/img/icon/back.png" style={{width: "30px", margin: "10px" }} /></Link>
                <a className="navbar-brand" href="#">&nbsp;&nbsp;Detail Produk</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                </div>
            </nav>
            <br></br>
            <div className="container">
            {product.map((item, key) => {
                return (
                    <div className="row">
                        <div className="col-sm-4 product-image">
                            <img src={`/assets/img/product/${item.image}`} style={{ width: "100%"}} alt="" />
                        </div>
                    <div className="col-sm-8">
                        <ul>
                            <li>Nama Produk : { item.product_name }</li>
                            <li>Deskripsi : { item.product_description }</li>
                            <li>Harga : { item.product_price }</li>
                        </ul>
                    </div>
                </div>
                )})}
            </div>
            <br />
            <br />
            <ProductImage id={this.props.match.params.id} />
        </React.Fragment>
    )
  }
}

export default ProductDetail
