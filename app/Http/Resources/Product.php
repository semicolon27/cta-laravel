<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product_name'        => $this->product_name,
            'product_description'       => $this->product_description,
            'product_price'       => $this->product_price,
            'image'       => $this->image,
        ];
    }
}
