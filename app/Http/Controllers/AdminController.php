<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Service;
use App\Models\ProductImage;
use App\Http\Resources\Product as PrResource;
use App\Http\Resources\Product as SvResource;
use Intervention\Image\Image;

class AdminController extends Controller
{
    //
    function getProduct(){
        $data = Product::get();
        return view('Dashboad/product', []);
    }

    // function getProductDetail($id){
    //     $data = Product::findOrFail($id);
    //     $img = ProductImage::where('product_id', $id)->get();
    //     $data = ['product' => [$data, 'image'=> $img]];
    //     return response()->json($data);
    // }

    function getImage($id){
        $img = ProductImage::where('product_id', $id)->get();
        $data = ['image' => $img];
        return response()->json($data);
    }

    function postImage($id, Request $r){
        if($r->get('file'))
       {
          $image = $r->get('file');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($r->get('file'))->save(public_path('assets/img/product/').$name);
        }
        $data = ProductImage::create([
            'img_id' => $r->img_id,
            'product_id' => $id,
            'img' => $name,
            'note' => $r->note
        ]);

        return (new PrResource($data))
                ->response()
                ->setStatusCode(201);
        return $r->all();
    }

    function addProduct(Request $r){
        if($r->get('file'))
       {
          $image = $r->get('file');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($r->get('file'))->save(public_path('assets/img/product/').$name);
        }

        $data = Product::create([
            'product_id' => $r->product_id,
            'product_name' => $r->product_name,
            'product_description' => $r->product_description,
            'product_price' => $r->product_price,
            'image' => $name
        ]);

        return (new PrResource($data))
                ->response()
                ->setStatusCode(201);
        return $r->all();
    }

    function putProduct($id, Request $r){
        $data = Product::find($id);
        $data->product_name = $r->product_name;
        $data->product_description = $r->product_description;
        $data->product_price = $r->product_price;
        $data->image = $r->image;
        $data->save();

        return (new PrResource($data))
                ->response()
                ->setStatusCode(201);
        // return $r->all();
    }

    function getService(){
        $data = Service::get();
        $data = ['results'=> $data];
        return response()->json($data);
    }

    function addService(Request $r){
        $data = Service::create($r->all());

        return (new SvResource($data))
                ->response()
                ->setStatusCode(201);
    }

    function putService($id, Request $r){
        $data = Service::find($id);
        $data->svc_name = $r->svc_name;
        $data->svc_desc = $r->svc_desc;
        $data->image = $r->image;
        $data->save();

        return (new SvResource($data))
                ->response()
                ->setStatusCode(201);
        // return $r->all();
    }

    function deleteProduct(Request $r){
        $data = Product::find($r->id);
        $data->delete();
        return response()->json($data);
    }

    function deleteService(Request $r){
        $data = Service::find($r->id);
        $data->delete();
        return response()->json($data);
    }

    function deleteImg(Request $r){
        $data = ProductImage::where('product_id', $r->id)->delete();
        return response()->json($data);
    }
}
