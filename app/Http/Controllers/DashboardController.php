<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Service;
use App\Models\ProductImage;

class DashboardController extends Controller
{
    //
    function getProduct(){
        $data = Product::get();
        return view('Dashboad/product', []);
    }

    function getProductById($id){
        $data = Product::findOrFail($id);
        $img = ProductImage::where('product_id', $id)->get();
        $data = ['product' => [$data], 'image'=> $img];
        return response()->json($data);
    }

    function getService(){
        $data = Service::get();
        $data = ['results'=> $data];
        return response()->json($data);
    }
}
