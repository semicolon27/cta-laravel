<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $r)
    {
        $check = Admin::where('username', $r->input('username'))
            ->where('password', $r->input('password'))
            ->first();

        if ($check != null) {
            // Login berhasil
            $r->session()->put('login_id', $check->username);

            return redirect('/admin/product');
        } else {
            $r->session()->put('login_id', null);
            $r->session()->flash('error', 'Username and/or Password Incorrect!');
            return redirect()->action('LoginController@loginpage');
        }
    }
    public function loginpage()
    {
        return view('login');
    }
    public function logout(Request $r)
    {
        $r->session()->forget('login_id');
        return redirect()->action('LoginController@loginpage');
    }
}
