<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Service;
use App\Models\ProductImage;
use App\Models\Message;
use App\Http\Resources\Message as MsgResource;


class HomeController extends Controller
{
    //
    function getProduct(){
        $data = Product::get();
        $data = ['results'=> $data];
        return response()->json($data);
    }

    function getProductById($id){
        $data = Product::findOrFail($id);
        $img = ProductImage::where('product_id', $id)->get();
        $data = ['product' => [$data], 'image'=> $img];
        return response()->json($data);
    }

    function getProductByLimit($num){
        $data = Product::take($num)->get();
        $data = ['results' => $data];
        return response()->json($data);
    }

    function getService(){
        $data = Service::get();
        $data = ['results'=> $data];
        return response()->json($data);
    }

    // function addMessage(Request $r){
    //     $data = new Message;
    //     $r = json_decode($r);
    //     $data->name = $r->name;
    //     $data->email = $r->email;
    //     $data->message = $r->message;
    //     $data->save();
    // }

    function addMessage(Request $r){
        $msg = Message::create($r->all());

        return (new MsgResource($msg))
                ->response()
                ->setStatusCode(201);
    }
}
