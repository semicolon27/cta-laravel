<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Admin;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public static $loggedUser = null;
    public function handle($request, Closure $next)
    {
        $login_id = $request->session()->get('login_id');
        if ($login_id == null) {
            return abort(404);
        }

        $logindata = Admin::find($login_id);
        if ($login_id == null) {
            return abort(404);
        }



        self::$loggedUser = $logindata;

        return $next($request);
    }
}
