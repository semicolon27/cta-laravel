<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "message";
    // public $incrementing = false;
    protected $primaryKey = "message_id";
    public $timestamps = false;
    protected $fillable = ['message_id', 'name', 'email', 'message'];
}
