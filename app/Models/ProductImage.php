<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    //
    protected $table = "product_image";
    protected $primaryKey = "img_id";
    public $timestamps = false;
    protected $fillable = ['img_id', 'product_id', 'img', 'note'];
}
