<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $table = "service";
    protected $primaryKey = "id_service";
    public $timestamps = false;
    protected $fillable = ['id_service', 'svc_name', 'svc_desc', 'image'];
}
