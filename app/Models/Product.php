<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "product";
    public $incrementing = false;
    protected $primaryKey = "product_id";
    public $timestamps = false;
    protected $fillable = ['product_id', 'product_name', 'product_description', 'product_price', 'image'];
}
