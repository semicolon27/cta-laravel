<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //
    protected $fillable = ['username', 'password'];
    protected $table = "admin";
    public $incrementing = false;
    protected $primaryKey = "username";
    public $timestamps = false;
}
