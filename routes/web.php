<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@loginpage');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::group(['middleware' => ['login']], function () {
    foreach(['/admin/{path?}', '/admin/product/{path?}'] as $r){
        Route::view($r, 'app');
    }
});
    foreach(['/', '/home', '/product', '/detail/{id}', '/service', '/about', '/contact'] as $r){
        Route::get($r, function () {
            View::addExtension('html', 'php');
            return View::make("index");
        });
    Route::get('/api/gallery/{id}', 'AdminController@getImage');
    Route::post('/api/product/gallery/{id}', 'AdminController@postImage');
    Route::post('/api/product', 'AdminController@addProduct');
    Route::put('/api/product/{id}/edit', 'AdminController@putProduct');
    Route::post('/api/service', 'AdminController@addService');
    Route::put('/api/service/{id}/edit', 'AdminController@putService');
    Route::post('/api/service', 'AdminController@addService');
    Route::post('/api/product/delete', 'AdminController@deleteProduct');
    Route::post('/api/service/delete', 'AdminController@deleteService');
    Route::post('/api/image/delete', 'AdminController@deleteImg');
    Route::get('/logout', 'LoginController@logout');
}

Route::get('/api/product', 'HomeController@getProduct');
Route::get('/api/product/{id}', 'HomeController@getProductById');
Route::get('/api/product/limit/{num}', 'HomeController@getProductByLimit');
Route::get('/api/service', 'HomeController@getService');
Route::post('/api/add_message', 'HomeController@addMessage');



